﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System.IO.Compression;
using System.Diagnostics;

namespace Hyperion_Pack_Launcher
{
    public class Program
    {
        // Minecraft Hyperion Pack Launcher
        // Written and maintained by AGuyWhoIsBored
        
        // Changelog:
        //      -v1.0: initial release
        //      -v1.1: changed installation process to be more robust and added launcher link
        //      -v1.2: delegated library installation process to Forge installer - much easier and allows for different versions of forge to be used
        //      -v1.2.1: fix bug in auto-update feature of launcher
        //      -v1.2.2: improve cleanup of forge installer

        static readonly string programVersion = "1.2.2";
        static bool hasInternetAccess = false;
        static readonly WebClient client = new WebClient();
        static ProgressBar progress;
        public static string modpackDataLink = "https://www.dropbox.com/s/p6wpg8j8hsrptvw/packdetails.txt?dl=1";
        public static string launcherLink = "https://www.dropbox.com/s/r0hszoycb4u0e8h/Hyperion%20Pack%20Launcher.exe?dl=1";

        // filesystem 
        public static string directoryInstall = null;
        public static string fileInstall = null;

        // modpack metadata
        public static string packVersion = null;
        public static string packName = null;
        public static string packModList = null;
        public static string packMCVersion = null;
        public static string acquiredDLDirect = null;
        public static string acquiredForgeDirect = null;

        public static void Main()
        {
            string userCommand = "";

            // introduction
            Console.Title = "Hyperion Modpack Launcher v" + programVersion;
            Console.WriteLine("----------------");
            Console.WriteLine("Hyperion Modpack Launcher [v" + programVersion + "]");
            Console.WriteLine("Developed by AGuyWhoIsBored");
            Console.WriteLine("----------------");
            Console.WriteLine("");

            // checking for internet access
            Console.WriteLine("Checking for internet access ...");
            if (checkForInternet("https://www.google.com")) { Console.WriteLine("The Hyperion Modpack Launcher was able to connect to the Internet!"); hasInternetAccess = true; }
            else
            {
                Console.WriteLine("The Hyperion Modpack Launcher was not able to connect to the internet!");
                Console.WriteLine("Some features may be limited!"); hasInternetAccess = false;
            }
            Console.WriteLine("");

        // ask for command
        TopOfLauncher:
            Console.WriteLine("Command line ready - waiting for command ...");
            Console.WriteLine("Use 'help' for help");
            userCommand = Console.ReadLine();

            // check for commands - do it this way so that we can also have flags
            // lists all available commands
            #region help command
            if (userCommand.Contains("help"))
            {
                Console.WriteLine("Commands: (* indicate required flags)");
                Console.WriteLine("[pull]: will pull all of the latest modpack data from the cloud and display that info");
                Console.WriteLine("[update]: will automatically update launcher to the latest version");
                Console.WriteLine("[setup] [-dfv]: will download and install specified version of the modpack to your Minecraft directory");
                Console.WriteLine("   -f: select which zip file to install");
                Console.WriteLine("   -d: select which folder to install to");
                Console.WriteLine(" * -v: select which version to download");
                Console.WriteLine("[download] [-dv]: will download specified version of the modpack to the selected directory");
                Console.WriteLine("   -d: select which folder to download modpack to");
                Console.WriteLine(" * -v: select which version to download from the cloud");
                Console.WriteLine("[install] [-df]: will install the selected version of the modpack to the selected directory");
                Console.WriteLine(" * -d: select where to install modpack");
                Console.WriteLine(" * -f: select which modpack zip file to install");
                Console.WriteLine("[launch]: will launch the Minecraft client for you to play on");
                Console.WriteLine("[changevar]: use this command to change internal program variables");
                Console.WriteLine("   -s1: change the modpack metadata server link");
                Console.WriteLine("   -s2: change the launcher server download link");
                Console.WriteLine("[quit]: will quit the launcher");
                Console.WriteLine("");
                goto TopOfLauncher;
            }
            #endregion
            // pulls latest versions and data from server
            #region pull command
            else if (userCommand.Contains("pull"))
            {
                if (hasInternetAccess)
                {
                    Console.WriteLine("Beginning pull from server ...");
                    Console.WriteLine("Checking connection to server ...");
                    if (checkForInternet("https://www.dropbox.com"))
                    {
                        Console.WriteLine("Connection to server established!");
                        Console.WriteLine("Downloading modpack metadata from server...");

                        // downloading
                        Task.Run(async () => await downloadFile(modpackDataLink, "packinfo.txt")).Wait();
                        Console.WriteLine("Download complete! Reading metadata ...");
                        // read downloaded data
                        string line;
                        StreamReader sr = new StreamReader(Environment.CurrentDirectory + "\\packinfo.txt");
                        while ((line = sr.ReadLine()) != null) { Console.WriteLine(line); }
                        sr.Close();
                        File.Delete(Environment.CurrentDirectory + "\\packinfo.txt");
                        Console.WriteLine("");
                        goto TopOfLauncher;
                    }
                }
                else { Console.WriteLine("Cannot pull data from server without Internet access!"); Console.WriteLine("Please check ur Internet connection and then relaunch the launcher!"); Console.WriteLine(""); goto TopOfLauncher; }
            }
            #endregion
            // checks for update to launcher
            #region checkupdate command
            else if (userCommand.Contains("update"))
            {
                if (hasInternetAccess)
                {
                    try
                    {
                        if(launcherLink == null) { Console.WriteLine("No link for launcher set!"); Console.WriteLine(""); goto TopOfLauncher; }
                        else
                        {
                            Console.WriteLine("Downloading latest launcher executable ...");
                            Task.Run(async () => await downloadFile(launcherLink, Environment.CurrentDirectory + "\\launcher.exe")).Wait();
                            Console.WriteLine("Download complete!");
                            Console.WriteLine("Launching new launcher ...");
                            Process.Start(Environment.CurrentDirectory + "\\launcher.exe");
                        }
                    }
                    catch (Exception e) { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
                }
                else { Console.WriteLine("Cannot check for updates without Internet access!"); Console.WriteLine("Please check ur Internet connection and then relaunch the launcher!"); Console.WriteLine(""); goto TopOfLauncher; }
            }
            #endregion
            // combination of download + install commands
            #region setup command
            else if (userCommand.Contains("setup"))
            {
                try
                {
                    #region check for flags and acquire flag data
                    int index = 0;
                    int flagCollectID = 0; // 1 - version | 2 - directoryinstall | 3 - fileinstall
                    int quoteCount = 0;
                    char currentChar;
                    string bufferString = null;

                    // check for flags and acquire data
                    userCommand = userCommand.Replace("setup", "");
                    userCommand += " ";
                    StringReader sr = new StringReader(userCommand);
                    while (index != userCommand.Length)
                    {
                        currentChar = (char)sr.Read();
                        index++;
                        bufferString += currentChar;
                        switch (currentChar)
                        {
                            case ' ':
                                if (bufferString.Contains("-v")) { flagCollectID = 1; }
                                else if (bufferString.Contains("-d")) { flagCollectID = 2; }
                                else if (bufferString.Contains("-f")) { flagCollectID = 3; }
                                else { if (flagCollectID == 1) { packVersion = bufferString; packVersion = RemoveWhitespace(packVersion); flagCollectID = 0; } }
                                if (quoteCount == 0) { bufferString = null; }
                                break;
                            case '"':
                                quoteCount++;
                                if (flagCollectID == 2 && quoteCount == 2) { directoryInstall = bufferString; directoryInstall = directoryInstall.Remove(directoryInstall.Length - 1); quoteCount = 0; flagCollectID = 0; }
                                else if (flagCollectID == 3 && quoteCount == 2) { fileInstall = bufferString; fileInstall = fileInstall.Remove(fileInstall.Length - 1); quoteCount = 0; flagCollectID = 0; }
                                bufferString = null;
                                break;
                        }
                    }
                    sr.Dispose();

                    // check for required flags and for syntax
                    if (string.IsNullOrWhiteSpace(packVersion)) { Console.WriteLine("Version flag is required!"); Console.WriteLine(""); goto TopOfLauncher; }
                    if (directoryInstall != null && !Directory.Exists(directoryInstall)) { Console.WriteLine("Provided directory doesn't exist!"); Console.WriteLine(""); goto TopOfLauncher; }
                    if (directoryInstall == null && !Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { Console.WriteLine("No directory given for install and default minecraft directory not found!"); Console.WriteLine(""); goto TopOfLauncher; }
                    else if (directoryInstall == null && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { directoryInstall = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft"; }
                    if (fileInstall != null && !File.Exists(fileInstall)) { Console.WriteLine("Provided zip file doesn't exist!"); Console.WriteLine(""); goto TopOfLauncher; }
                    #endregion

                    if (!hasInternetAccess && fileInstall == null) { Console.WriteLine("Cannot download modpack from server as there is no Internet connection! Please connect to the Internet and then relaunch the launcher!"); Console.WriteLine(""); goto TopOfLauncher; }
                    else { downloadPhase(packVersion, directoryInstall); }
                    installationPhase();

                    Console.WriteLine("Hyperion Modpack " + packVersion + " successfully installed to " + directoryInstall);
                    Console.WriteLine("Use the 'launch' command to launch Minecraft!");

                    // end of command
                    Console.WriteLine("");
                    goto TopOfLauncher;
                }
                catch (Exception e) { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
            }
            #endregion
            // downloads specified version of modpack to host computer
            #region download command
            else if (userCommand.Contains("download"))
            {
                #region check for flags and acquire flag data
                int index = 0;
                int flagCollectID = 0; // 1 - version | 2 - directoryinstall
                int quoteCount = 0;
                char currentChar;
                string bufferString = null;

                // check for flags and acquire data
                userCommand = userCommand.Replace("download", "");
                userCommand += " ";
                StringReader sr = new StringReader(userCommand);
                while (index != userCommand.Length)
                {
                    currentChar = (char)sr.Read();
                    index++;
                    bufferString += currentChar;
                    switch (currentChar)
                    {
                        case ' ':
                            if (bufferString.Contains("-v")) { flagCollectID = 1; }
                            else if (bufferString.Contains("-d")) { flagCollectID = 2; }
                            else { if (flagCollectID == 1) { packVersion = bufferString; packVersion = RemoveWhitespace(packVersion); flagCollectID = 0; } }
                            if (quoteCount == 0) { bufferString = null; }
                            break;
                        case '"':
                            quoteCount++;
                            if (flagCollectID == 2 && quoteCount == 2) { directoryInstall = bufferString; directoryInstall = directoryInstall.Remove(directoryInstall.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            bufferString = null;
                            break;
                    }
                }
                sr.Dispose();

                // check for required flags and for syntax
                if (string.IsNullOrWhiteSpace(packVersion)) { Console.WriteLine("Version flag is required!"); Console.WriteLine(""); goto TopOfLauncher; }
                if (directoryInstall != null && !Directory.Exists(directoryInstall)) { Console.WriteLine("Provided directory doesn't exist!"); Console.WriteLine(""); goto TopOfLauncher; }
                if (directoryInstall == null && !Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { Console.WriteLine("No directory given for install and default minecraft directory not found!"); Console.WriteLine(""); goto TopOfLauncher; }
                else if (directoryInstall == null && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { directoryInstall = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft"; }
                #endregion
                if (!hasInternetAccess) { Console.WriteLine("Cannot download modpack from server as there is no Internet connection! Please connect to the Internet and then relaunch the launcher!"); Console.WriteLine(""); goto TopOfLauncher; }
                else { downloadPhase(packVersion, directoryInstall); Console.WriteLine("Hyperion Modpack v" + packVersion + " downloaded to '" + directoryInstall + "'"); Console.WriteLine(""); goto TopOfLauncher; }
            }
            #endregion
            // installs selected version of modpack to %mc% folder
            #region install command
            else if (userCommand.Contains("install"))
            {
                try
                {
                    directoryInstall = null;
                    fileInstall = null;
                    #region check for flags and acquire flag data
                    int index = 0;
                    int flagCollectID = 0; // 1 - diretoryInstal; | 2 - fileInstall
                    int quoteCount = 0;
                    char currentChar;
                    string bufferString = null;

                    // check for flags and acquire data
                    userCommand = userCommand.Replace("download", "");
                    userCommand += " ";
                    StringReader sr = new StringReader(userCommand);
                    while (index != userCommand.Length)
                    {
                        currentChar = (char)sr.Read();
                        index++;
                        bufferString += currentChar;
                        switch (currentChar)
                        {
                            case ' ':
                                if (bufferString.Contains("-d")) { flagCollectID = 1; }
                                if (bufferString.Contains("-f")) { flagCollectID = 2; }
                                if (quoteCount == 0) { bufferString = null; }
                                break;
                            case '"':
                                quoteCount++;
                                if (flagCollectID == 1 && quoteCount == 2) { directoryInstall = bufferString; directoryInstall = directoryInstall.Remove(directoryInstall.Length - 1); quoteCount = 0; flagCollectID = 0; }
                                if (flagCollectID == 2 && quoteCount == 2) { fileInstall = bufferString; fileInstall = fileInstall.Remove(fileInstall.Length - 1); quoteCount = 0; flagCollectID = 0; }
                                bufferString = null;
                                break;
                        }
                    }
                    sr.Dispose();

                    // check for required flags and for syntax
                    if (string.IsNullOrWhiteSpace(directoryInstall) || string.IsNullOrWhiteSpace(fileInstall)) { Console.WriteLine("Directory and file flags are required!"); Console.WriteLine(""); goto TopOfLauncher; }
                    if (directoryInstall != null && !Directory.Exists(directoryInstall)) { Console.WriteLine("Provided directory doesn't exist!"); Console.WriteLine(""); goto TopOfLauncher; }
                    if (directoryInstall == null && !Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { Console.WriteLine("No directory given for install and default minecraft directory not found!"); Console.WriteLine(""); goto TopOfLauncher; }
                    else if (directoryInstall == null && Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft")) { directoryInstall = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\.minecraft"; }
                    if (fileInstall != null && !File.Exists(fileInstall)) { Console.WriteLine("Provided zip file doesn't exist!"); Console.WriteLine(""); goto TopOfLauncher; }
                    #endregion
                    installationPhase();

                    Console.WriteLine("Hyperion Modpack successfully installed to '" + directoryInstall + "'!"); Console.WriteLine(""); goto TopOfLauncher;
                }
                catch (Exception e) { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
            }
            #endregion
            // launches minecraft client
            #region launch command
            else if (userCommand.Contains("launch"))
            {
                try
                {
                    if (directoryInstall == null) { Console.WriteLine("No directory selected! Please make sure you either run the 'setup', 'download', or 'install' commands prior to launching Minecraft!"); Console.WriteLine(""); goto TopOfLauncher; }
                    Console.WriteLine("Checking for Minecraft executable ...");
                    if (!File.Exists(directoryInstall + "\\Minecraft.exe"))
                    {
                        Console.WriteLine("Minecraft executable not found! Downloading ...");
                        Task.Run(async () => await downloadFile("https://launcher.mojang.com/download/Minecraft.exe", directoryInstall + "\\Minecraft.exe")).Wait();
                    }
                    Console.WriteLine("Launching Minecraft launcher!");
                    Process.Start(directoryInstall + "\\Minecraft.exe");

                    Console.WriteLine("");
                    goto TopOfLauncher;
                }
                catch (Exception e) { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
            }
            #endregion
            // changes internal server variables
            #region changevar command
            else if (userCommand.Contains("changevar"))
            {
                if (!(userCommand.Contains("-s1")
                    || userCommand.Contains("-s2")))
                {
                    Console.WriteLine("No var selected to change!");
                    Console.WriteLine("");
                    goto TopOfLauncher;
                }

                int index = 0;
                int flagCollectID = 0; // 1 - modpack metadata link | 2 - launcher download link
                int quoteCount = 0;
                char currentChar;
                string bufferString = null;
                string s1 = null;
                string s2 = null;
                // check for flags and acquire data
                userCommand = userCommand.Replace("changevar", "");
                userCommand += " ";
                StringReader sr = new StringReader(userCommand);
                while (index != userCommand.Length)
                {
                    currentChar = (char)sr.Read();
                    index++;
                    bufferString += currentChar;
                    switch (currentChar)
                    {
                        case ' ':
                            if (bufferString.Contains("-s1")) { flagCollectID = 1; }
                            else if (bufferString.Contains("-s2")) { flagCollectID = 2; }
                            if (quoteCount == 0) { bufferString = null; }
                            break;
                        case '"':
                            quoteCount++;
                            if (flagCollectID == 1 && quoteCount == 2) { s1 = bufferString; s1 = s1.Remove(s1.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            else if (flagCollectID == 2 && quoteCount == 2) { s2 = bufferString; s2 = s2.Remove(s2.Length - 1); quoteCount = 0; flagCollectID = 0; }
                            bufferString = null;
                            break;
                    }
                }
                sr.Dispose();
                if (s1 != null) { modpackDataLink = s1; }
                if (s2 != null) { launcherLink = s2; }
                Console.WriteLine("var modpackDataLink updated to '" + modpackDataLink + "'");
                Console.WriteLine("var launcherLink updated to '" + launcherLink + "'");
                Console.WriteLine("");
                goto TopOfLauncher;

            }
            #endregion
            // quits application
            #region quit command
            else if (userCommand.Contains("quit"))
            {
                File.Delete(Environment.CurrentDirectory + "\\packinfo.txt");
                Environment.Exit(0);
            }
            #endregion
            else { Console.WriteLine("Unrecognized command!"); Console.WriteLine(""); goto TopOfLauncher; }
        }
        public static bool checkForInternet(string website)
        {
            try { using (var stream = client.OpenRead(website)) { return true; } }
            catch { return false; }
        }
        private static void downloadUpdated(object sender, DownloadProgressChangedEventArgs e) { progress.Report(e.ProgressPercentage / 100.00); }
        public static async Task downloadFile(string file, string location)
        {
            progress = new ProgressBar();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(downloadUpdated);
            await client.DownloadFileTaskAsync(new Uri(file), location);
            progress.Dispose();
        }
        public static string RemoveWhitespace(string str) { return new string(str.ToCharArray().Where(c => !char.IsWhiteSpace(c)).ToArray()); }
        public static void deleteRecursive(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs) { deleteRecursive(dir); }

            Directory.Delete(target_dir, false);
        }
        public static void downloadPhase(string version, string downloadDirectory)
        {
            try
            {
                // redownload pack info 
                Console.WriteLine("Downloading modpack metadata ...");
                Task.Run(async () => await downloadFile(modpackDataLink, downloadDirectory + "\\packinfo.txt")).Wait();

                if (fileInstall == null)
                {
                    Console.WriteLine("Downloading version [" + version + "] of the Hyperion Modpack...");
                    // grab dldirect link from file
                    // read downloaded data
                    string line;
                    bool foundVersion = false;
                    StreamReader sr1 = new StreamReader(downloadDirectory + "\\packinfo.txt");
                    while ((line = sr1.ReadLine()) != null)
                    {
                        if (line == ("v" + version))
                        {
                            foundVersion = true;
                            // collect and parse metadata
                            packVersion = line;
                            packName = sr1.ReadLine();
                            packModList = sr1.ReadLine();
                            packMCVersion = sr1.ReadLine();
                            acquiredDLDirect = sr1.ReadLine();
                            acquiredForgeDirect = sr1.ReadLine();

                            packName = packName.Replace("name ", "");
                            packModList = packModList.Replace("mods ", "");
                            packMCVersion = packMCVersion.Replace("mcver ", "");
                            acquiredDLDirect = acquiredDLDirect.Replace("dldirect ", "");
                            acquiredForgeDirect = acquiredForgeDirect.Replace("forgedirect ", "");

                            Console.WriteLine("Modpack metadata successfully acquired and parsed!");
                            break;
                        }
                    }
                    sr1.Close();
                    if (!foundVersion) { Console.WriteLine("Couldn't find specified version of the Hyperion Modpack!"); Console.ReadLine(); }

                    // downloading of file
                    Task.Run(async () => await downloadFile(acquiredDLDirect, downloadDirectory + "\\hyperion_v" + version + ".zip")).Wait();
                    Console.WriteLine("Download complete!");
                    fileInstall = downloadDirectory + "\\hyperion_v" + version + ".zip";
                }
                else { Console.WriteLine("Download skipped because zip directory already provided!"); }
                File.Delete(downloadDirectory + "\\packinfo.txt");
            }
            catch (Exception e) { Console.WriteLine("An error occured: " + e.Message); Console.ReadLine(); }
        }
        public static void installationPhase()
        {
            string folderNameDirectory = fileInstall.Remove(fileInstall.Length - 4);
            string folderNameSingle = new DirectoryInfo(folderNameDirectory).Name;
            // steps: unzip, move all folders included to directoryInstall directory, download and run forge installer as provided in packdetails.txt
            // check if to-be-unzipped folder exists; if it does, delete
            if (Directory.Exists(directoryInstall + "\\" + folderNameSingle)) { deleteRecursive(directoryInstall + "\\" + folderNameSingle); }
            Console.WriteLine("Unzipping modpack contents ...");
            // can't have progressbar for zipfile :( or at least the current way I'm doing it
            ZipFile.ExtractToDirectory(fileInstall, directoryInstall);
            File.Delete(fileInstall);

            // make sure all folders inside dldirect folder are moved to directoryInstall directory
            int folderCount = Directory.GetDirectories(directoryInstall + "\\" + folderNameSingle).Length;
            string[] subDirectoryEntries = Directory.GetDirectories(directoryInstall + "\\" + folderNameSingle);
            for (int i = 0; i < folderCount; i++) { subDirectoryEntries[i] = new DirectoryInfo(subDirectoryEntries[i]).Name; }
            if(folderCount == 1) { Console.WriteLine("[1] folder to update minecraft filestructure with"); }
            else if(folderCount > 1) { Console.WriteLine("[" + folderCount.ToString() + "] folders to update minecraft filestructure with"); }

            // run loop
            for (int i = 0; i < folderCount; i++)
            {
                Console.WriteLine("Updating minecraft filestructure ... [" + (i + 1).ToString() + "/" + folderCount.ToString() + "]");
                if (Directory.Exists(directoryInstall + "\\" + subDirectoryEntries[i]))
                {
                askBUP1:
                    Console.WriteLine("Existing '" + subDirectoryEntries[i] + "' folder found! Backup? Not backing up will remove the existing one! [y/n]");
                    if (Console.ReadKey(true).Key == ConsoleKey.Y) { Directory.Move(directoryInstall + "\\" + subDirectoryEntries[i], directoryInstall + "\\" + subDirectoryEntries[i] + "-backup[" + DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss") + "]"); }
                    else if (Console.ReadKey(true).Key == ConsoleKey.N) { Console.WriteLine("Not backing up, copying over existing one!"); Directory.Delete(directoryInstall + "\\" + subDirectoryEntries[i], true); }
                    else { Console.WriteLine("Unrecognized command!"); goto askBUP1; }
                }
                Directory.Move(directoryInstall + "\\" + folderNameSingle + "\\" + subDirectoryEntries[i], directoryInstall + "\\" + subDirectoryEntries[i]);
            }
            deleteRecursive(directoryInstall + "\\" + folderNameSingle);

            // checking if libraries are required
            Console.WriteLine("Checking if external library downloads are needed ...");
            if (!File.Exists(directoryInstall + "\\versions\\" + packMCVersion + "\\" + packMCVersion + ".json"))
            {
                Console.WriteLine("External library downloads are needed!");
                if (!hasInternetAccess) { Console.WriteLine("Cannot download external libraries! Please make sure you have an Internet connection, and then relaunch the launcher!"); Console.WriteLine(""); Console.ReadLine(); }
                else
                {
                    // delegate library installation process to the dedicated installer provided by Forge - how convenient
                    Console.WriteLine("Downloading required library installer...");
                    Task.Run(async () => await downloadFile(acquiredForgeDirect, Environment.CurrentDirectory + "\\forge-installer.jar")).Wait();
                    Console.WriteLine("Running library installer, the Hyperion Modpack installer will continue when the installer finishes execution ...");
                    Process.Start(Environment.CurrentDirectory + "\\forge-installer.jar").WaitForExit();

                    // library installer cleanup
                    File.Delete(Environment.CurrentDirectory + "\\forge-installer.jar");
                    if (File.Exists(Environment.CurrentDirectory + "\\forge-installer.jar.log")) File.Delete(Environment.CurrentDirectory + "\\forge-installer.jar.log");
                    Console.WriteLine("Installation of external libraries complete!");
                }
            }
            else Console.WriteLine("External library downloads are not needed!");
            // end of function
        }
    }
}
